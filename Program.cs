﻿using Cafeteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cafeteria
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        public static void Menu()
        {
            Console.WriteLine("Menu");

            Console.WriteLine("1) Buscar producto");
            Console.WriteLine("2) Crear producto");
            Console.WriteLine("3) Eliminar producto");
            Console.WriteLine("4) Actualizar producto");
            Console.WriteLine("0) Salir");
            Console.Write("Selecione una opción: ");
            string opcion = Console.ReadLine();

            switch (opcion)
            {
                case "1":
                    BuscarProductos();
                    break;
                case "2":
                    CrearProducto();
                    break;
                case "3":
                    EliminarProducto();
                    break;
                case "4":
                    ActualizarProducto();
                    break;
                case "0": return;
            }
            Menu();
        }

        public static void CrearProducto()
        {
            Console.WriteLine("Crear producto");
            Productos producto = new Productos();
            producto = LlenarProducto(producto);
            using (CafeteriaContext context = new CafeteriaContext())
            {
                context.Add(producto);
                context.SaveChanges();
                Console.WriteLine("Producto creado :)");
            }
        }

        public static void BuscarProductos()
        {
            Console.WriteLine("Buscar productos");
            Console.Write("Buscar: ");
            string buscar = Console.ReadLine();

            using (CafeteriaContext context = new CafeteriaContext())
            {
                IQueryable<Productos> productos = context.Productos.Where(p => p.Nombre.Contains(buscar));
                foreach (Productos producto in productos)
                {
                    Console.WriteLine($"{producto.Id}) {producto.Nombre}");
                }
            }
        }

        public static Productos SelecionarProducto()
        {
            BuscarProductos();
            Console.Write("Selecione código de producto: ");
            int id = int.Parse(Console.ReadLine());
            using (CafeteriaContext context = new CafeteriaContext())
            {
                Productos producto = context.Productos.Find(id);
                if (producto == null)
                {
                    SelecionarProducto();
                }
                return producto;
            }
        }

        public static Productos LlenarProducto(Productos producto)
        {
            Console.Write("Nombre: ");
            producto.Nombre = Console.ReadLine();
            Console.Write("Descripción: ");
            producto.Descripcion = Console.ReadLine();
            Console.Write("Precio: ");
            producto.Precio = decimal.Parse(Console.ReadLine());
            Console.Write("Costo: ");
            producto.Costo = decimal.Parse(Console.ReadLine());
            Console.Write("Cantidad: ");
            producto.Cantidad = decimal.Parse(Console.ReadLine());
            Console.Write("Tamaño: ");
            producto.Tamano = Console.ReadLine();
            return producto;
        }

        public static void ActualizarProducto()
        {
            Console.WriteLine("Actualizar producto");
            Productos producto = SelecionarProducto();
            producto = LlenarProducto(producto);
            using (CafeteriaContext context = new CafeteriaContext())
            {
                context.Update(producto);
                context.SaveChanges();
                Console.WriteLine("Producto actualizado :)");
            }
        }

        public static void EliminarProducto()
        {
            Console.WriteLine("Eliminar producto");
            Productos producto = SelecionarProducto();
            using (CafeteriaContext context = new CafeteriaContext())
            {
                context.Remove(producto);
                context.SaveChanges();
                Console.WriteLine("Producto eliminado :)");
            }
        }
    }
}
