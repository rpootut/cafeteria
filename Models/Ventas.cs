﻿using System;
using System.Collections.Generic;

namespace Cafeteria.Models
{
    public partial class Ventas
    {
        public Ventas()
        {
            Detalles = new HashSet<Detalles>();
        }

        public int Id { get; set; }
        public decimal? Total { get; set; }
        public DateTime? Fecha { get; set; }
        public string Cliente { get; set; }

        public virtual ICollection<Detalles> Detalles { get; set; }
    }
}
