﻿using System;
using System.Collections.Generic;

namespace Cafeteria.Models
{
    public partial class Productos
    {
        public Productos()
        {
            Detalles = new HashSet<Detalles>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Tamano { get; set; }
        public decimal? Costo { get; set; }
        public decimal? Precio { get; set; }
        public decimal? Cantidad { get; set; }

        public virtual ICollection<Detalles> Detalles { get; set; }
    }
}
