﻿using System;
using System.Collections.Generic;

namespace Cafeteria.Models
{
    public partial class Detalles
    {
        public int Id { get; set; }
        public int? ProductoId { get; set; }
        public int? VentaId { get; set; }
        public decimal? Total { get; set; }

        public virtual Productos Producto { get; set; }
        public virtual Ventas Venta { get; set; }
    }
}
